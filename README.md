# Karaoke Mugen CI/CD Docker image

This is NOT a Karaoke Mugen docker image.

This image is used for continuous integration/developement 

## Buildx

To build for several archs at once, use buildx:

```sh
docker buildx build --push --platform linux/arm64/v8,linux/arm/v7,linux/amd64 --tag user/image:tag .
```
